/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "LowPassFilter.h"
#include "HighpassFilter.h"
#include "ButterworthFilter.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public AudioDeviceManager, //inherit from audiodevicemanager
                        public AudioIODeviceCallback, //inherit from audioIOdeivcecallback
                        public SliderListener



                        //public ButtonListener
                        //public AudioDeviceSelectorComponent
                        //public DocumentWindow

{
/* */
public:
    //==============================================================================
    
    /* */
    MainComponent();
    
   
    void closeButtonPressed()
    {
        delete this;
    }
    /* */
    ~MainComponent();
    
    /* */
    void resized() override;
    
    /* override the pure virtual funtions */
    void audioDeviceIOCallback(const float** inputChannelData,
                               int numInputChannels,
                               float** outputChannelData,
                               int numOutputChannels,
                               int numSamples) override;
    
    /* */
    void audioDeviceAboutToStart(AudioIODevice* device) override;
    /* */
    void audioDeviceStopped() override;
    
    /* */
    void sliderValueChanged	(Slider * 	slider1);
    
    //int indexOfItemId (int itemId);
    
    int getSelectedId	(		);

    
    //void resized(AudioDeviceSelectorComponent* settings);
    
    //void timerCallback(AudioDeviceSelectorComponent* settings);

/* */
private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
    /* */
    AudioDeviceManager audioDeviceManager;
    //AudioDeviceSelectorComponent settings;
    
    //Slider Component
    Slider slider1;
    
    //Lowpass
    Slider gainSlider;
    Slider mixSlider;
    Slider feedbackGainSlider;
    
    //Highpass
    Slider gainHighSlider;
    Slider mixHighSlider;
    Slider feedbackHighGainSlider;
    
    //Filter Component
    LowpassFilter lowpassFilter;
    HighpassFilter highpassFilter;
    ButterworthFilter butterworthFilter;
    
    //Unusued (yet to use)
    //DocumentWindow  channel1;
    
    //Labels
    //Lowpass
    Label gainLabel;
    Label mixLabel;
    Label feedbackLabel;
    
    //Highpass
    Label gainHighLabel;
    Label mixHighLabel;
    Label feedbackHighLabel;
    
    //Private Variables
    float gain;
    float mix;
    float Highgain;
    float Highmix;
    
    ComboBox filterType;
 
    
};


#endif  // MAINCOMPONENT_H_INCLUDED
