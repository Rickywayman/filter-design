/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
/* MainComponent Constructor - this is where your components are added to the program */
MainComponent::MainComponent()
{
    setSize (400, 600);
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    audioDeviceManager.addAudioCallback(this); //main component to receive audio callbacks
    
   //Lowpass
    //slider setup
    addAndMakeVisible(&gainSlider);
    gainSlider.setRange(0.0, 1.0, 0.01);
    gainSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    gainSlider.addListener(this);
    gainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    //gainSlider.setValue(0.8, true);
    
    addAndMakeVisible(&mixSlider);
    mixSlider.setRange(0.0, 1.0, 0.01);
    mixSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    mixSlider.addListener(this);
    mixSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    //mixSlider.setValue(0.2, true);
    
    addAndMakeVisible(&feedbackGainSlider);
    feedbackGainSlider.setRange(0.0, 1.0, 0.01);
    feedbackGainSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    feedbackGainSlider.addListener(this);
    feedbackGainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    //feedbackGainSlider.setValue(0.5, true);
    
    //Labels
    gainLabel.setText("gain", dontSendNotification);
    gainLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&gainLabel);
    
    mixLabel.setText("wet/dry mix", dontSendNotification);
    mixLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&mixLabel);
    
    feedbackLabel.setText("feedback", dontSendNotification);
    feedbackLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&feedbackLabel);
    
    //Highpass
    //slider setup
    addAndMakeVisible(&gainHighSlider);
    gainHighSlider.setRange(0.0, 1.0, 0.01);
    gainHighSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    gainHighSlider.addListener(this);
    gainHighSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    //gainSlider.setValue(0.8, true);
    
    addAndMakeVisible(&mixHighSlider);
    mixHighSlider.setRange(0.0, 1.0, 0.01);
    mixHighSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    mixHighSlider.addListener(this);
    mixHighSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    //mixSlider.setValue(0.2, true);
    
    addAndMakeVisible(&feedbackHighGainSlider);
    feedbackHighGainSlider.setRange(0.0, 1.0, 0.01);
    feedbackHighGainSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    feedbackHighGainSlider.addListener(this);
    feedbackHighGainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    //feedbackGainSlider.setValue(0.5, true);
    
    //Labels
    gainHighLabel.setText("gain", dontSendNotification);
    gainHighLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&gainHighLabel);
    
    mixHighLabel.setText("wet/dry mix", dontSendNotification);
    mixHighLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&mixHighLabel);
    
    feedbackHighLabel.setText("feedback", dontSendNotification);
    feedbackHighLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&feedbackHighLabel);
    
    addAndMakeVisible(filterType);
    filterType.addItem("Lowpass", 1);
    filterType.addItem("Highpass", 2);

    
}
/* */
MainComponent::~MainComponent()
{
    audioDeviceManager.removeAudioCallback(this); //ensure main component stops listening to audio callbacks when closed.
    
}
/* */
void MainComponent::resized()
{
    //slider1.setBounds(55, 300, 200, 80);
    
    //int x = getWidth();
    //    int y = getHeight();
    
    int comboSize = (getWidth()-20)/3;
    if(comboSize > (getHeight()-60)) comboSize = getHeight() - 60;

    filterType.setBounds(10, 165, comboSize, 30);
    
    //Lowpass
    int sliderSize = (getWidth()-20)/3;
    if(sliderSize > (getHeight()-60)) sliderSize = getHeight() - 60;
    
    gainLabel.setBounds(10, 0, sliderSize, 20);
    mixLabel.setBounds(10+sliderSize, 0, sliderSize, 20);
    feedbackLabel.setBounds(10+sliderSize+sliderSize, 0, sliderSize, 20);
    
    gainSlider.setBounds(10, 20, sliderSize, sliderSize);
    mixSlider.setBounds(10+sliderSize, 20, sliderSize, sliderSize);
    feedbackGainSlider.setBounds(10+sliderSize+sliderSize, 20, sliderSize, sliderSize);
    
    //Highpass
    int sliderHighSize = (getWidth()-20)/3;
    if(sliderHighSize > (getHeight()-60)) sliderHighSize = getHeight() - 60;
    
    gainHighLabel.setBounds(10, 200, sliderSize, 20);
    mixHighLabel.setBounds(10+sliderSize, 200, sliderSize, 20);
    feedbackHighLabel.setBounds(10+sliderSize+sliderSize, 200, sliderSize, 20);
    
    gainHighSlider.setBounds(10, 220, sliderSize, sliderSize);
    mixHighSlider.setBounds(10+sliderSize, 220, sliderSize, sliderSize);
    feedbackHighGainSlider.setBounds(10+sliderSize+sliderSize, 220, sliderSize, sliderSize);
    
}
/* */
void MainComponent::audioDeviceAboutToStart(AudioIODevice* device)
{
    DBG("audioDeviceAboutToStart");
    float getCurrentSampleRate(AudioIODevice* device);
}
/* */
void MainComponent::audioDeviceStopped()
{
    DBG("audio device stopped");
}


int MainComponent::getSelectedId()
{
    int type;

    type = filterType.getSelectedId();
    
    return type;
}



//SliderCallback================================================================
void MainComponent::sliderValueChanged (Slider* slider)
{
    
    //Lowpass
    
    if (slider == &gainSlider)
    {
        
        gain = slider->getValue();
        gain = gain*gain*gain; //cubic rule
        
    }
    else if (slider == &mixSlider)
    {
        
        mix = slider->getValue();
        
    }
    else if (slider == &feedbackGainSlider)
    {
        
        float feedbackGain = slider->getValue();
        feedbackGain = feedbackGain*feedbackGain*feedbackGain;
        lowpassFilter.setFeedbackGain(feedbackGain);
        
    }
    
    
    //Hipass
    
    if (slider == &gainHighSlider)
    {
        
        Highgain = slider->getValue();
        Highgain = Highgain*Highgain*Highgain; //cubic rule
        
    }
    else if (slider == &mixHighSlider)
    {
        
        Highmix = slider->getValue();
        
    }
    else if (slider == &feedbackHighGainSlider)
    {
        
        float feedbackHighGain = slider->getValue();
        feedbackHighGain = feedbackHighGain*feedbackHighGain*feedbackHighGain;
        highpassFilter.setFeedbackGain(feedbackHighGain);
        
    }

}



/* Audio Callback - Audio Processing takes place here @ samplerate */
void MainComponent::audioDeviceIOCallback(const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)

{
 const float *inL = inputChannelData[0];
 const float *inR = inputChannelData[1];
 float *outL = outputChannelData[0];
 float *outR = outputChannelData[1];
    
    
 
 while(numSamples--)
 {
     float output = *inL;
     
     
     
     if (filterType.getSelectedId() == 1)
     {
         output = lowpassFilter.filter(output) * gain;
     }
     else if(filterType.getSelectedId() == 2)
     {
         output = (highpassFilter.filter(output) * Highgain) + *inL;
     }
     
     
     
     
     *outL = output;
     *outR = output;
     
    
     
     inL++;
     inR++;
     outL++;
     outR++;
     
 }
 

};



