//
//  LowPassFilter.cpp
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 12/01/2016.
//
//

#include "LowPassFilter.h"


LowpassFilter::LowpassFilter()
{
    setDelayInSamples(1.f);
    
    
}

LowpassFilter::~LowpassFilter()
{
    
}

//==============================================================================

float LowpassFilter::filter(float input)
{
    float delayLineOutput = delayLineRead();
    
    float output = (feedbackGain * input) + ((1.f-feedbackGain) * delayLineOutput);
    
    delayLineWrite(output);
    
    return output;
}
