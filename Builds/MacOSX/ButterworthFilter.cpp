//
//  ButterworthFilter.cpp
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 13/01/2016.
//
//

#include "ButterworthFilter.h"


ButterworthFilter::ButterworthFilter()
{
    setDelayInSamples(1.f);
}

ButterworthFilter::~ButterworthFilter()
{
    
}

//==============================================================================

//float ButterworthFilter::filter(float input)
//{
    
  // }

//     yv[0] =   (ax[0] * xv[0] + ax[1] * xv[1] + ax[2] * xv[2]

float ButterworthFilter::filter(float forwardinput)
{
    
    // float delayLineOutput = delayLineRead();
    float feedforward = forwarddelayLineRead();
    
    //float output = (feedbackGain * input) + ((1.f-feedbackGain) * delayLineOutput);
    
    float output = (feedbackGain * forwardinput) + ((1.f-feedbackGain) * feedforward);
    
    //delayLineWrite(output);
    forwarddelayLineWrite(forwardinput);
    
    return output;

    
}