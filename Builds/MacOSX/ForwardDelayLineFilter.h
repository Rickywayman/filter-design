//
//  ForwardDelayLineFilter.h
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 13/01/2016.
//
//

#ifndef __JuceBasicWindow__ForwardDelayLineFilter__
#define __JuceBasicWindow__ForwardDelayLineFilter__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"


#define ASSUMEDSAMPLERATE 44100

class ForwardDelayLineFilter
{
public:
    //==============================================================================
    /**
     Constructor
     */
    ForwardDelayLineFilter();
    
    /**
     Destructor
     */
    virtual ~ForwardDelayLineFilter();
    //==============================================================================
    /**
     Set the delay time in samples of the delayline - can be fractional
     */
    void setforwardDelayInSamples(float delayInSamples);
    
    /**
     Sets the feedback gain
     */
    void setforwardFeedbackGain(float val);
    
    /**
     Function to perform the filter processing should call delayLineRead and delayLineWrite at some point
     in the process
     */
    virtual float forwardfilter(float forwardinput)=0;
    
protected:
    /**
     Reads from the delayline using linear interpolation - should be called once per filter call and prior
     to delayLineWrite or the delay will be one sample inacurate
     */
    float forwarddelayLineRead();
    
    /**
     Writes to the delayline - should be called once per filter call and after to delayLineRead or
     the delay will be one sample inacurate
     */
    void forwarddelayLineWrite(float input);
    float forwardfeedbackGain;
    
private:
    float forwarddelayLine[ASSUMEDSAMPLERATE];
    float forwardreadDelaySamples;
    int forwardwriteIndex;
    
    
    
};



#endif /* defined(__JuceBasicWindow__ForwardDelayLineFilter__) */
